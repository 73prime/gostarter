# What is this?

This is a simple script that will build up a Go project that is functional.

The directory structure will look like:

```
- <programname>\go.mod
- <programname>\Makefile
- <programname>\Dockerfile
- <programname>\src\main.go
- <programname>\src\main_test.go
```

## What does it do?

It leverages:
* Docker
* Unit Tests
* HTTP Post example
* Logrus

### To use
* Initing: `./gostarter.sh <programname> <rootfolder>`
* Running locally: `make run`
* Running via docker: `make docker`
* Doing tests: `make showcoverage`

Once running, you can see how to POST both JSON data, and form data:
* JSON parsing: `curl -H "Content-Type: application/json" http://127.0.0.1:5000 -d '{"text":"test string"}'`
* Regular form parsing: `curl http://127.0.0.1:5000 -d 'text="test"'`