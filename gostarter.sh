#!/bin/bash

programname=$1
location=$2

function printUsage() {
    echo "This needs 2 parameters: "
    echo "1: the name of the project"
    echo "2: the folder location of the project"
    echo "NOTE: this will create a project in the location specified, with a folder named after your project"
}
if [[ -z $programname || -z $location ]]; then
    printUsage
    exit 1
fi

newdirectory=$location/$programname
if [ -d "$newdirectory" ]; then
    echo "$newdirectory is a directory, and already exists...exiting."
    exit 1
fi

mkdir -p $newdirectory && cd $newdirectory

# go.mod
cat >> go.mod << EOF
module $programname

go 1.18
require (
	github.com/sirupsen/logrus v1.9.0 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
EOF
# go.sum
cat >> go.sum << EOF
github.com/davecgh/go-spew v1.1.0/go.mod h1:J7Y8YcW2NihsgmVo/mv3lAwl/skON4iLHjSsI+c5H38=
github.com/davecgh/go-spew v1.1.1/go.mod h1:J7Y8YcW2NihsgmVo/mv3lAwl/skON4iLHjSsI+c5H38=
github.com/pmezard/go-difflib v1.0.0/go.mod h1:iKH77koFhYxTK1pcRnkKkqfTogsbg7gZNVY4sRDYZ/4=
github.com/sirupsen/logrus v1.9.0 h1:trlNQbNUG3OdDrDil03MCb1H2o9nJ1x4/5LYw7byDE0=
github.com/sirupsen/logrus v1.9.0/go.mod h1:naHLuLoDiP4jHNo9R0sCBMtWGeIprob74mVsIT4qYEQ=
github.com/stretchr/objx v0.1.0/go.mod h1:HFkY916IF+rwdDfMAkV7OtwuqBVzrE8GR6GFx+wExME=
github.com/stretchr/testify v1.7.0/go.mod h1:6Fq8oRcR53rry900zMqJjRRixrwX3KX962/h/Wwjteg=
golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 h1:0A+M6Uqn+Eje4kHMK80dtF3JCXC4ykBgQG4Fe06QRhQ=
golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8/go.mod h1:oPkhp1MJrh7nUepCBck5+mAzfO9JrbApNNgaTdGDITg=
gopkg.in/check.v1 v0.0.0-20161208181325-20d25e280405/go.mod h1:Co6ibVJAznAaIkqp8huTwlJQCZ016jof/cbN4VW5Yz0=
gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c/go.mod h1:K4uyk7z7BCEPqu6E+C64Yfv1cQ7kz7rIZviUmN+EgEM=
EOF

# Dockerfile
cat >> Dockerfile << EOF
FROM golang:alpine
RUN apk add --update make
RUN mkdir /app
ADD . /app/
WORKDIR /app
RUN make build
CMD ["./bin/$programname"]
EOF
mkdir -p src

# Makefile
cat >> Makefile << EOF
.PHONY: default
default: displayhelp ;

displayhelp:
	@echo Use "clean, showcoverage, tests, build, docker or run" with make, por favor.

showcoverage: tests
	@echo Running Coverage output
	go tool cover -html=coverage.out

tests: clean
	@echo Running Tests
	go test --coverprofile=coverage.out ./...

run: build
	@echo Running program
	LOG_LEVEL=DEBUG ./bin/$programname

build: clean
	@echo Running build command
	go build -o bin/$programname src/main.go

clean:
	@echo Removing binary TODO
	rm -rf ./bin ./vendor Gopkg.lock

docker:
	docker build -t $programname:latest . -f Dockerfile
	docker run -it -p 5000:5000 $programname:latest

EOF

# main.go
cat >> src/main.go << EOF
package main

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strconv"
	"strings"
)

var port int

func adder(x, y int) int {
	return x + y
}
func init() {
	initlogging()
	initConfig()
}
func initlogging() {
	//Trace, Debug, Info, Warn, Error, Fatal, and Panic are valid
	logfmt := os.Getenv("LOG_FORMAT")
	if strings.ToUpper(logfmt) == "JSON" {
		log.SetFormatter(&log.JSONFormatter{})
	} else {
		log.SetFormatter(&log.TextFormatter{
			DisableColors: false,
			FullTimestamp: true,
		})
	}
	lvl, ok := os.LookupEnv("LOG_LEVEL")
	// LOG_LEVEL not set, let's default to debug
	if !ok {
		lvl = "debug"
	}
	// parse string, this is built-in feature of logrus
	ll, err := log.ParseLevel(lvl)
	if err != nil {
		ll = log.DebugLevel
	}
	// set global log level
	log.SetLevel(ll)
	log.Debug("Finished configuring logger.")
	log.Warn("========++++REMOVE THE BELOW STUFF++++========")
	log.WithFields(log.Fields{
		"animal": "walrus",
		"size":   10,
	}).Info("A group of walrus emerges from the ocean")

	// A common pattern is to re-use fields between logging statements by re-using
	// the logrus.Entry returned from WithFields()
	contextLogger := log.WithFields(log.Fields{
		"common": "this is a common field",
		"other":  "I also should be logged always",
	})
	contextLogger.Info("I'll be logged with common and other field")
	contextLogger.Info("Me too")
	log.Warn("========++++REMOVE THE ABOVE STUFF STUFF++++========")
}
func initConfig() {
	port = 5000
	portstr := os.Getenv("PORT")
	porti, err := strconv.Atoi(portstr)
	if err == nil {
		port = porti
	}
	log.Debugf("Finished initConfig, port %d", port)
}
func RootHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		response := fmt.Sprintf("Verb %s not implemented.", r.Method)
		http.Error(w, response, http.StatusInternalServerError)
		return
	}
	if len(r.Header["Content-Type"]) != 1 {
		response := fmt.Sprintf("Header needs to be defined.")
		http.Error(w, response, http.StatusInternalServerError)
		return
	}
	var text string
	if r.Header["Content-Type"][0] == "application/json" {
		//curl -H "Content-Type: application/json" http://127.0.0.1:5000 -d '{"text":"test string"}
		type text_struct struct {
			Text string \`json:"text"\`
		}
		decoder := json.NewDecoder(r.Body)
		var t text_struct
		err := decoder.Decode(&t)
		if err == nil {
			text = t.Text
			log.Infof("Text from POST (via JSON payload) is: %s.", text)
		}
	} else if r.Header["Content-Type"][0] == "application/x-www-form-urlencoded" {
		//curl http://127.0.0.1:5000 -F 'text="test string"'
		r.ParseForm()
		text = r.FormValue("text")
		log.Infof("Text from POST (via form values) is: %s.", text)
	} else {
		response := fmt.Sprintf("Header %s not implemented.", r.Header["Content-Type"][0])
		log.Errorf(response)
		http.Error(w, response, http.StatusInternalServerError)
		return
	}
	//no errors thus far, return 200.
	data, _ := json.Marshal(map[string]string{
		"text": fmt.Sprintf("Returning your POSTed text: %s", text),
	})
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	w.Write(data)

}

func handleLocalRequests() {
	http.HandleFunc("/", RootHandler)
	log.Printf("Started Webserver on port %d.\n", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), nil))
}
func main() {
	log.Info("$programname API started")
	handleLocalRequests()
}

EOF

# main_test.go
cat >> src/main_test.go << EOF
package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
)

func setupMain() {
}
func teardownMain() {
}
func TestMain(m *testing.M) {
	setupMain()
	code := m.Run()
	teardownMain()
	os.Exit(code)
}
func TestAdder(t *testing.T) {
	expected := 5
	actual := adder(2, 3)
	if expected != actual {
		t.Errorf("Expected %d, got %d", expected, actual)
	}
}

func TestRootHandler(t *testing.T) {
	//testing POST-only
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	w := httptest.NewRecorder()
	RootHandler(w, req)
	res := w.Result()
	defer res.Body.Close()
	if res.StatusCode != http.StatusInternalServerError {
		t.Errorf("Expected Status error %v, but got %v", http.StatusInternalServerError, res.StatusCode)
	}

	//testing empty header
	req = httptest.NewRequest(http.MethodPost, "/", nil)
	w = httptest.NewRecorder()
	RootHandler(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != http.StatusInternalServerError {
		t.Errorf("Expected Status error %v, but got %v", http.StatusInternalServerError, res.StatusCode)
	}

	//testing empty data, but bad header
	req = httptest.NewRequest(http.MethodPost, "/", nil)
	req.Header.Add("Content-Type", "application/jsonmisspelled")
	w = httptest.NewRecorder()
	RootHandler(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != http.StatusInternalServerError {
		t.Errorf("Expected Status code %v, but got %v", http.StatusInternalServerError, res.StatusCode)
	}

	//testing JSON data
	payload := strings.NewReader(\`{
    	"text" : "Test String"
	}\`)
	req = httptest.NewRequest(http.MethodPost, "/", payload)
	req.Header.Add("Content-Type", "application/json")
	w = httptest.NewRecorder()
	RootHandler(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected Status code %v, but got %v", http.StatusOK, res.StatusCode)
	}
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	expected := "{\"text\":\"Returning your POSTed text: Test String\"}"
	if string(data) != expected {
		t.Errorf("expected %v got %v", expected, string(data))
	}

	//testing Form data
	payload = strings.NewReader(\`text="Test String"\`)
	req = httptest.NewRequest(http.MethodPost, "/", payload)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	w = httptest.NewRecorder()
	RootHandler(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected Status code %v, but got %v", http.StatusOK, res.StatusCode)
	}
	data, err = ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	expected = \`{"text":"Returning your POSTed text: \"Test String\""}\`
	if string(data) != expected {
		t.Errorf("expected %v got %v", expected, string(data))
	}
}
EOF